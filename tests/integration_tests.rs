use tbytes::errors::TBytesError;
use tbytes::{TBytesReader, TBytesReaderFor, TBytesWriter, TBytesWriterFor};

// The contents of this test must be copied to `README.md` and `src/lib/mod.rs` examples.
#[test]
fn example_read_write() -> Result<(), TBytesError> {
    type Content = (i16, f32, [u8; 2], [i16; 2]);
    let mut buffer = [0u8; 12];

    let mut writer = TBytesWriter::from(buffer.as_mut_slice());

    let into_values: Content = (-1048, 0.32, [10, 31], [-1, 240]);
    writer.write(into_values.0)?;
    writer.write(into_values.1)?;
    writer.write_slice(into_values.2.as_slice())?;
    writer.write_array(into_values.3)?;

    assert!(matches!(writer.write(0u8), Err(TBytesError::OutOfBounds)));

    let reader = TBytesReader::from(buffer.as_slice());

    let mut from_values: Content = Content::default();
    from_values.0 = reader.read()?;
    from_values.1 = reader.read()?;
    from_values.2 = reader.read_array()?;
    from_values.3 = reader.read_array()?;

    assert!(matches!(
        reader.read() as Result<u8, TBytesError>,
        Err(TBytesError::OutOfBounds)
    ));

    assert_eq!(into_values, from_values);

    Ok(())
}

#[test]
fn basic_read_write() {
    type Content = (u8, i8, i16, f32, [u8; 2], [i16; 2]);
    let mut buffer = [0u8; 14];

    let mut writer = TBytesWriter::from(buffer.as_mut_slice());

    let into_values: Content = (10, -20, 1048, 0.32, [10, 31], [-1, 240]);
    writer.write(into_values.0).unwrap();
    writer.write(into_values.1).unwrap();
    writer.write(into_values.2).unwrap();
    writer.write(into_values.3).unwrap();
    writer.write_slice(into_values.4.as_slice()).unwrap();
    writer.write_array(into_values.5).unwrap();

    assert!(matches!(writer.write(0u8), Err(TBytesError::OutOfBounds)));

    let reader = TBytesReader::from(buffer.as_slice());

    let mut from_values: Content = Content::default();
    from_values.0 = reader.read().unwrap();
    from_values.1 = reader.read().unwrap();
    from_values.2 = reader.read().unwrap();
    from_values.3 = reader.read().unwrap();
    from_values.4 = reader.read_array().unwrap();
    from_values.5 = reader.read_array().unwrap();

    assert!(matches!(
        reader.read() as Result<u8, TBytesError>,
        Err(TBytesError::OutOfBounds)
    ));

    assert_eq!(into_values, from_values);
}
