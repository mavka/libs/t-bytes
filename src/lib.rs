//! # T-Bytes
//!
//! T-Bytes is a tiny library for reading and writing typed data into bytes buffers. It is designed
//! mainly for `no-std`, `no-alloc` targets or crates which consider to support for `no-std`.
//!
//! # Usage
//!
//! It would be easier to start from an example. Here we create a buffer and populate it with
//! heterogeneous values.
//!
//! ```rust
//! use tbytes::errors::TBytesError;
//! use tbytes::{TBytesWriterFor, TBytesReader, TBytesReaderFor, TBytesWriter};
//!
//! fn main() -> Result<(), TBytesError> {
//!     type Content = (i16, f32, [u8; 2], [i16; 2]);
//!     let mut buffer = [0u8; 12];
//!
//!     let mut writer = TBytesWriter::from(buffer.as_mut_slice());
//!
//!     let into_values: Content = (-1048, 0.32, [10, 31], [-1, 240]);
//!     writer.write(into_values.0)?;
//!     writer.write(into_values.1)?;
//!     writer.write_slice(into_values.2.as_slice())?;
//!     writer.write_array(into_values.3)?;
//!
//!     assert!(matches!(writer.write(0u8), Err(TBytesError::OutOfBounds)));
//!
//!     let reader = TBytesReader::from(buffer.as_slice());
//!
//!     let mut from_values: Content = Content::default();
//!     from_values.0 = reader.read()?;
//!     from_values.1 = reader.read()?;
//!     from_values.2 = reader.read_array()?;
//!     from_values.3 = reader.read_array()?;
//!
//!     assert!(matches!(
//!         reader.read() as Result<u8, TBytesError>,
//!         Err(TBytesError::OutOfBounds)
//!     ));
//!
//!     assert_eq!(into_values, from_values);
//!
//!     Ok(())
//! }
//! ```
//!
//! # Limitations
//!
//! * At the moment only `little endian` is supported.
//! * We currently supports only numeric types and arrays of such types. But you can always
//!   implement your own [`TBytesReaderFor`] / [`TBytesWriterFor`]. Or even better, submit a
//!   [pull request](https://gitlab.com/mavka/libs/t-bytes/-/merge_requests)!
#![warn(missing_docs)]
#![deny(rustdoc::broken_intra_doc_links)]
#![doc(
    html_logo_url = "https://gitlab.com/mavka/libs/t-bytes/-/raw/main/avatar.png?ref_type=heads",
    html_favicon_url = "https://gitlab.com/mavka/libs/t-bytes/-/raw/main/avatar.png?ref_type=heads"
)]
#![cfg_attr(not(feature = "std"), no_std)]

pub mod bytes_reader;
pub use bytes_reader::{TBytesReader, TBytesReaderBackend, TBytesReaderFor};
pub mod bytes_writer;
pub use bytes_writer::{TBytesWriter, TBytesWriterBackend, TBytesWriterFor};

pub mod errors;
