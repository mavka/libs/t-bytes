//! # Errors.
//!
//! We currently emit quite a few errors. The most important one [`TBytesError::OutOfBounds`]
//! emitted when reader/writer reached the end of the buffer.  

/// T-Bytes errors.
#[derive(Clone, Copy, Debug)]
pub enum TBytesError {
    /// Buffer exhausted.
    OutOfBounds,
}
