T-Bytes
=======

<img src="https://gitlab.com/mavka/libs/t-bytes/-/raw/main/avatar.png?ref_type=heads" alt="logo" width="64" height="64" title="Byte em all!"/>

T-Bytes is a tiny library for reading and writing typed data into bytes buffers. It is designed mainly for `no-std`,
`no-alloc` targets or crates which consider to support `no-std`.

Documentation can be found [here](https://docs.rs/tbytes/latest/tbytes/).

Installation
------------

```shell
cargo add tbytes
```

Usage
-----

It would be easier to start from an example. Here we create a buffer and populate it with heterogeneous values.

```rust
use tbytes::errors::TBytesError;
use tbytes::{BytesWriterFor, TBytesReader, TBytesReaderFor, TBytesWriter};

fn main() -> Result<(), TBytesError> {
    type Content = (i16, f32, [u8; 2], [i16; 2]);
    let mut buffer = [0u8; 12];
    
    let mut writer = TBytesWriter::from(buffer.as_mut_slice());
    
    let into_values: Content = (-1048, 0.32, [10, 31], [-1, 240]);
    writer.write(into_values.0)?;
    writer.write(into_values.1)?;
    writer.write_slice(into_values.2.as_slice())?;
    writer.write_array(into_values.3)?;
    
    assert!(matches!(writer.write(0u8), Err(TBytesError::OutOfBounds)));
    
    let reader = TBytesReader::from(buffer.as_slice());
    
    let mut from_values: Content = Content::default();
    from_values.0 = reader.read()?;
    from_values.1 = reader.read()?;
    from_values.2 = reader.read_array()?;
    from_values.3 = reader.read_array()?;
    
    assert!(matches!(
          reader.read() as Result<u8, TBytesError>,
          Err(TBytesError::OutOfBounds)
      ));
    
    assert_eq!(into_values, from_values);
    
    Ok(())
}
```

See [examples](#examples) for advanced usage.

Examples
--------

- [`basic`](./examples/basic.rs) — basic read/write.
  ```shell
  cargo run --example basic
  ```

Limitations
-----------

We want to keep as simple as possible but simplicity comes with a price. 

* At the moment only `little endian` is supported.
* We currently supports only numeric types and arrays of such types. But you can always
  implement your own [`TBytesReaderFor`](src/bytes_reader.rs) / [`TBytesWriterFor`](src/bytes_writer.rs). Or even
  better, submit a [pull request](https://gitlab.com/mavka/libs/t-bytes/-/merge_requests)!

License
-------

> Here we simply comply with the suggested dual licensing according to
> [Rust API Guidelines](https://rust-lang.github.io/api-guidelines/about.html) (C-PERMISSIVE).

Licensed under either of

* Apache License, Version 2.0
  ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
* MIT license
  ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

Contribution
------------

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
